setInterval(tick, 100)

let audioContext = new AudioContext()
let checkBeat = 0
let bpm = document.getElementById('bpmInput').value

console.log(bpm)
function updateBPM(){
    bpm = document.getElementById('bpmInput').value 
    console.log(bpm)
}

function tick() {
    //update html clock
    let fTime = getFancyTime(4, bpm, audioContext.currentTime)
    document.getElementById('timer').innerHTML = fTime
    if (checkBeat != fTime[1]){
        checkBeat = fTime[1]
        // console.log('changed to beat ' + fTime[1])
        scheduleClick(fTime)
    }
    //
}

function getFancyTime(beatsPerBar, bpm, time) {
    measures = parseInt(1 + (time / bpmToNoteDurration(bpm)) / beatsPerBar)
    beats = parseInt(1 + (time / bpmToNoteDurration(bpm)) % beatsPerBar)
    sixtyFourths = 1 + (parseInt(time / bpmToNoteDurration(bpm) * 64) % 64)
    let fTime = [measures, beats, sixtyFourths]
    return fTime
}
function bpmToNoteDurration(bpm) {
    return 60 / bpm
}
function scheduleClick(fTime) {
    let bar = fTime[0] + 1
    let beat = fTime[1]
    let sixtyFourth = 1
    let fTimeToSchedule = [bar, beat, sixtyFourth]
    let osc = audioContext.createOscillator()
    console.log(beat)
    if(beat == 1){
        osc.frequency.setValueAtTime(1200, audioContext.currentTime)
    }else{
        osc.frequency.setValueAtTime(800, audioContext.currentTime)
    }
    osc.connect(audioContext.destination)
    let startTime = getBeatTime(fTimeToSchedule, 4, bpm)
    osc.start(startTime)
    osc.stop(startTime + 0.04)
    // console.log('scheduling beat ' + bar + " " + beat + " " + sixtyFourth + "  :: Current is " + fTime)
}

function getBeatTime(fTime, beatsPerBar, bpm) {
    let measures = fTime[0] - 1
    //              measures * (top of Time Signature) +   beats   + extra 64th notes
    let beats = (measures * beatsPerBar) + fTime[1] - 1 + ((fTime[2] - 1) / 64)
    //             ( # of Beats * 60 sec ) / BPM
    let beatTime = (beats * 60) / bpm
    return beatTime
}




